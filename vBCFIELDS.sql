  CREATE OR REPLACE FORCE VIEW "SIEBEL"."VDEV_BCFIELDS"  AS 
  SELECT
      T1.row_id                           AS BC_ID,
      T1.NAME                             AS BC,
      f1.name                             AS field,
      f1.row_id                           AS field_id,
      t.name AS TAB,
      t.ROW_ID as TAB_ID,
      f1.picklist_name,
      DECODE(f1.join_name, t.name, '', f1.join_name) as join_name,
      f1.col_name,
      c.ROW_ID as col_id,
      f1.mvlink_name,
      T1.Repository_Id,
      R1.NAME AS Repository
FROM Siebel.S_Buscomp T1
   INNER JOIN Siebel.S_Field F1    ON F1.Buscomp_Id = T1.Row_Id
   inner JOIN Siebel.s_Repository R1    ON T1.Repository_Id = R1.Row_Id
   LEFT OUTER JOIN siebel.s_join j ON(f1.join_name  = j.name AND j.buscomp_id = t1.row_id) and NVL(j.INACTIVE_FLG ,'N') <> 'Y'
   left outer join siebel.S_BUSCOMP_UPROP BUP on BUP.BUSCOMP_ID =T1.ROW_ID and BUP.NAME = 'Inner Join Extension Table 1'
   left outer join siebel.s_table t on t.name = NVL(j.dest_tbl_name, NVL(BUP.VALUE,t1.table_name)) and t.Repository_Id = r1.ROW_ID
    left outer join siebel.s_column c on c.name = f1.col_name and c.tbl_ID = t.row_id   
WHERE NVL(T1.INACTIVE_FLG ,'N') <> 'Y' 
  and NVL(F1.INACTIVE_FLG ,'N') <> 'Y' 
  and NVL(j.INACTIVE_FLG ,'N') <> 'Y'
 -- and t1.name = '&1' and T1.REPOSITORY_ID = getrepositoryid() and f1.name like '&2%'
;