CREATE OR REPLACE VIEW APPLETCON AS
SELECT a.name,
       a.row_id        AS Applet_id,
       c.name          AS control,
       C.FIELD_NAME    AS FIELD,
       A.buscomp_name,
       BC.ROW_ID       AS BC_ID,
       awt.type        AS applet_mode_cd,
       BC.TABLE_NAME,
       a.REPOSITORY_ID,
       R.Name          AS Repository
  FROM SIEBEL.S_APPLET A
 INNER JOIN SIEBEL.S_REPOSITORY R ON R.ROW_ID = A.REPOSITORY_ID
 INNER JOIN siebel.S_buscomp bc   ON bc.name = A.buscomp_name AND R.ROW_ID = BC.REPOSITORY_ID
 INNER JOIN siebel.S_CONTROL c    ON c.applet_id = A.ROW_ID
 INNER JOIN siebel.S_APPL_WEB_TMPL awt ON awt.applet_id = a.row_id
 INNER JOIN siebel.S_APPL_WTMPL_IT awti ON awti.appl_web_tmpl_id = awt.row_id AND awti.ctrl_name = c.name
  WHERE (a.INACTIVE_FLG <> 'Y' OR a.INACTIVE_FLG IS NULL)
   AND (bc.INACTIVE_FLG <> 'Y' OR bc.INACTIVE_FLG IS NULL)
   AND (c.INACTIVE_FLG <> 'Y' OR c.INACTIVE_FLG IS NULL)
   AND (awt.INACTIVE_FLG <> 'Y' OR awt.INACTIVE_FLG IS NULL)
   AND (awti.INACTIVE_FLG <> 'Y' OR awti.INACTIVE_FLG IS NULL)
UNION 
SELECT a.name,
       a.row_id        AS Applet_id,
       c.name          AS control,
       C.FIELD_NAME    AS FIELD,
       A.buscomp_name,
       BC.ROW_ID       AS BC_ID,
       awt.type        AS applet_mode_cd,
       BC.TABLE_NAME,
       a.REPOSITORY_ID,
       R.Name          AS Repository
  FROM SIEBEL.S_APPLET A
 INNER JOIN SIEBEL.S_REPOSITORY R  ON R.ROW_ID = A.REPOSITORY_ID
 INNER JOIN siebel.S_buscomp bc    ON bc.name = A.buscomp_name AND R.ROW_ID = BC.REPOSITORY_ID
 INNER JOIN siebel.S_LIST L        ON L.applet_id = A.ROW_ID
 INNER JOIN siebel.S_LIST_COLUMN C ON C.list_id = L.ROW_ID
 INNER JOIN siebel.S_APPL_WEB_TMPL awt ON awt.applet_id = a.row_id
 INNER JOIN SIEBEL.S_APPL_WTMPL_IT awtf   ON awtf.appl_web_tmpl_id = awt.ROW_ID AND awtf.ctrl_name = c.name
 WHERE (a.INACTIVE_FLG <> 'Y' OR a.INACTIVE_FLG IS NULL)
   AND (bc.INACTIVE_FLG <> 'Y' OR bc.INACTIVE_FLG IS NULL)
   AND (l.INACTIVE_FLG <> 'Y' OR l.INACTIVE_FLG IS NULL)
   AND (c.INACTIVE_FLG <> 'Y' OR c.INACTIVE_FLG IS NULL)
   AND (awt.INACTIVE_FLG <> 'Y' OR awt.INACTIVE_FLG IS NULL)
   AND (awtf.INACTIVE_FLG <> 'Y' OR awtf.INACTIVE_FLG IS NULL)
