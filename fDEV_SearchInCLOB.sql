create or replace function DEV_SearchInCLOB(vscript CLOB, vSearch varchar)
   return varchar2 is
   vRet varchar2(32000);
   iPos number := 1;

   function cut(vscript CLOB, iPos number) return varchar2 is
   begin
      return(to_char(substr(vscript,
                            iPos,
                            instr(vscript, ';', iPos) - iPos + 1)));
   end cut;
   
begin
   iPos := instr(vscript, vSearch, iPos);
   if iPos > 0 then
      vRet := cut(vscript,iPos); 

      --iPos := iPos + length(vRet);
   else
      vRet := 'NOTFOUND';
   end if;
   return(to_char(iPos) || ': ' || vRet);
end DEV_SearchInCLOB;
/
