CREATE OR REPLACE VIEW DEV_IOFIELDS AS
SELECT IO.NAME          AS INTOBJ,
       IC.NAME          AS INTBC,
       IC.EXT_NAME      AS BC,
       BC.ROW_ID        AS BC_ID,
       ICF.NAME         AS INTFIELD,
       ICF.ROW_ID       AS INTFIELD_ID,
       ICF.EXT_NAME     AS BCFIELD,
       IO.REPOSITORY_ID,
       R.NAME           AS REPOSITORY 
  FROM SIEBEL.S_INT_OBJ IO
 INNER JOIN SIEBEL.S_REPOSITORY R ON R.ROW_ID = IO.REPOSITORY_ID
 INNER JOIN SIEBEL.S_INT_COMP IC    ON IC.INT_OBJ_ID = IO.ROW_ID
 INNER JOIN SIEBEL.S_INT_FIELD ICF  ON ICF.INT_COMP_ID = IC.ROW_ID
 INNER JOIN SIEBEL.S_BUSCOMP BC     ON BC.NAME = IC.EXT_NAME AND IC.REPOSITORY_ID = BC.REPOSITORY_ID
WHERE (IO.INACTIVE_FLG <> 'Y' OR IO.INACTIVE_FLG IS NULL)
   AND (IC.INACTIVE_FLG <> 'Y' OR IC.INACTIVE_FLG IS NULL)
   AND (ICF.INACTIVE_FLG <> 'Y' OR ICF.INACTIVE_FLG IS NULL)   
   AND (BC.INACTIVE_FLG <> 'Y' OR BC.INACTIVE_FLG IS NULL)

/* ---
SELECT INT_OBJ_ID, IBC_ID, IBC, UK_NAME  , SUBSTR(MAX(SYS_CONNECT_BY_PATH(FIELD_NAME, ' | ')), 4) , REPOSITORY_ID
FROM (   
         SELECT UKF.REPOSITORY_ID, IC.INT_OBJ_ID, IC.ROW_ID AS IBC_ID, IC.NAME AS IBC, UK.NAME AS UK_NAME 
              , UKF.NAME as FIELD_NAME, row_number() OVER(PARTITION BY UK.ROW_ID ORDER BY UK.ROW_ID) NodeDepth
           FROM SIEBEL.S_INT_CKEY_FLD UKF
         inner join SIEBEL.S_INT_CKEY UK ON UKF.INT_KEY_ID = UK.ROW_ID
         inner join SIEBEL.S_INT_COMP IC ON IC.ROW_ID = UK.INT_COMP_ID
         inner join SIEBEL.S_INT_OBJ IO ON IC.INT_OBJ_ID = IO.ROW_ID
         WHERE IO.NAME = '&1' AND IO.Repository_Id = '&2'
          AND (UK.INACTIVE_FLG <> 'Y' OR UK.INACTIVE_FLG IS NULL)   
            AND (UKF.INACTIVE_FLG <> 'Y' OR UKF.INACTIVE_FLG IS NULL)
            AND (IO.INACTIVE_FLG <> 'Y' OR IO.INACTIVE_FLG IS NULL)
            AND (IC.INACTIVE_FLG <> 'Y' OR IC.INACTIVE_FLG IS NULL)
            order by UK.ROW_ID, UKF.Seq_Num
) START WITH NodeDepth = 1
                 CONNECT BY PRIOR UK_NAME = UK_NAME AND PRIOR NodeDepth = NodeDepth - 1
                  GROUP BY INT_OBJ_ID, IBC, IBC_ID, UK_NAME, REPOSITORY_ID  
                  order by   IBC, UK_NAME 
--- */