CREATE OR REPLACE VIEW vDEV_SMART_SCRIPT_DOC AS
WITH ANSWR AS  (SELECT quest_id, VALUE, VALUE || '=>' || answer AS ANSWR_TC
                  FROM siebel.S_CS_ANSWR a
                  LEFT OUTER JOIN Siebel.S_CS_ANSWR_LANG l ON a.row_id = l.answr_id),
 PinSSS As (SELECT SSS.row_id as script_id, page.row_id as page_id, page.name
										FROM SIEBEL.S_CS_PATH sss
										LEFT OUTER JOIN SIEBEL.S_CS_EDGE T5 ON T5.PATH_ID = sss.Row_Id
										INNER      JOIN SIEBEL.S_CS_PAGE page     ON (T5.NEXT_PAGE_ID = page.ROW_ID OR sss.start_page_id = page.ROW_ID)
										LEFT OUTER JOIN SIEBEL.S_CS_EDGE T40      ON T40.PAGE_ID = page.row_id
										INNER      JOIN SIEBEL.S_CS_QUEST q       ON (T40.FROM_QUEST_ID = q.ROW_ID OR page.start_quest_id = q.ROW_ID)
									group by SSS.row_id, page.row_id, page.name),
 QinP as ( SELECT
									distinct T4.PAGE_ID , NVL(T2.row_id,T3.row_id) as q_id, NVL(T2.name,T3.name) as q_name
							 FROM
									 SIEBEL.S_CS_ANSWR T1,
									 SIEBEL.S_CS_QUEST T2,
									 SIEBEL.S_CS_QUEST T3,
									 SIEBEL.S_CS_EDGE T4
							 WHERE
									T4.FROM_QUEST_ID = T2.ROW_ID (+) AND
									T4.NEXT_QUEST_ID = T3.ROW_ID (+) AND
									T4.FROM_ANSWR_ID = T1.ROW_ID (+))
SELECT DISTINCT sss.name AS SmartScript,
                sssl.label AS LABEL,
                sss.row_id AS sss_row_id,
                (SELECT 'n=' || COUNT(*) || ' ' || SUBSTR(MAX(SYS_CONNECT_BY_PATH(NAME, ', ')), 3)
                   FROM (SELECT path_id,
                                NAME,
                                row_number() OVER(PARTITION BY path_id ORDER BY path_id) NodeDepth
                           FROM siebel.s_cs_path_scpt ORDER BY NAME) x
                  WHERE x.path_id = sss.row_id
                  START WITH NodeDepth = 1
                 CONNECT BY PRIOR path_id = path_id
                        AND PRIOR NodeDepth = NodeDepth - 1
                  GROUP BY path_id) AS escript4sss,
                page.name AS sss_page,
                pl.Label AS page_lable,
                page.row_id AS page_row_id,
                q.NAME AS question_1,
                q.row_id AS q_row_id,
                ql.question AS question_2,
                q.save_field,
                q.save_user_params || ' (' || q.answer_format_cd || ')' AS save_user_params,
                (SELECT 'n=' || COUNT(*) || ' ' || SUBSTR(MAX(SYS_CONNECT_BY_PATH(NAME, ', ')), 3)
                   FROM (SELECT quest_id, NAME, row_number() OVER(PARTITION BY quest_id ORDER BY quest_id) NodeDepth
                           FROM siebel.s_cs_quest_scpt ORDER BY NAME) y
                  WHERE y.quest_id = q.row_id
                  START WITH NodeDepth = 1
                 CONNECT BY PRIOR quest_id = quest_id AND PRIOR NodeDepth = NodeDepth - 1
                  GROUP BY quest_id) AS escript4q,
                q.auto_subst_params,
                (SELECT SUBSTR(MAX(SYS_CONNECT_BY_PATH(ANSWR_TC, ' | ')), 4)
                   FROM (SELECT quest_id, ANSWR_TC, row_number() OVER(PARTITION BY quest_id ORDER BY quest_id) NodeDepth
                           FROM ANSWR  ORDER BY ANSWR_TC)
                  WHERE quest_id = q.row_id
                  START WITH NodeDepth = 1
                 CONNECT BY PRIOR quest_id = quest_id
                        AND PRIOR NodeDepth = NodeDepth - 1
                  GROUP BY quest_id) AS answers,
									'UTIL.SetPageQuestionValueAndRO("'||page.name||'", "'||q.NAME||'", "VALUE", true);' as 		ESCRIPT_HELPER,
									DECODE(sss.start_page_id, page.row_id, 0, 1)||page.name||DECODE(page.Start_Quest_Id, q.row_id, 0, DECODE(substr(q.name,-6,6),'FOOTER',9,1)) as o
									,(SELECT page_id from QinP WHERE q_id = sss.start_quest_id) b, sss.start_page_id, sss.start_quest_id

  FROM SIEBEL.S_CS_PATH sss
  INNER JOIN siebel.s_cs_path_lang sssl ON sssl.path_id = sss.row_id
	inner join PinSSS                     on PinSSS.script_id = sss.row_id
  INNER JOIN SIEBEL.S_CS_PAGE page      ON page.row_id = PinSSS.page_id
	inner join QinP                       on page.row_id = QinP.page_id
  INNER JOIN SIEBEL.S_CS_QUEST q        ON q.row_id = QinP.q_id
  LEFT OUTER JOIN siebel.s_cs_quest_lang ql ON ql.quest_id = q.row_id
  LEFT OUTER JOIN siebel.s_cs_page_lang pl  ON pl.page_id = page.row_id
  ---where sss.name = 'LMS Forgot Secret Type'
;
