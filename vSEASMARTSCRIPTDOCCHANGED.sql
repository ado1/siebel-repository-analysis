CREATE OR REPLACE VIEW SEA_SMART_SCRIPT_DOC_CHANGED AS
WITH sss_changed AS (
SELECT sss.name AS SmartScript,
       nvl((SELECT MAX(x.last_upd) FROM siebel.s_cs_path_scpt x WHERE x.path_id = sss.row_id)
           , to_date('01011980','DDMMYYYY')) AS LUpd_path_scpt,
       nvl((SELECT MAX(y.last_upd) FROM siebel.s_cs_quest_scpt y WHERE y.quest_id = q.row_id)
           ,to_date('01011980','DDMMYYYY')) AS LUpd_quest_scpt,
       nvl((SELECT MAX(a.last_upd) FROM siebel.S_CS_ANSWR a WHERE a.quest_id = q.row_id),
           to_date('01011980','DDMMYYYY')) AS LUpd_ANSWR,
       ----------------
       SSS.Last_Upd  AS LUpd_PATH,
       nvl(T5.Last_Upd,to_date('01011980','DDMMYYYY'))  AS LUpd_EDGE1,
       page.last_upd AS LUpd_PAGE,
       nvl(t40.last_upd,to_date('01011980','DDMMYYYY')) AS LUpd_EDGE2,
       q.last_upd    AS LUpd_QUEST,
       pl.last_upd   AS LUpd_QUEST_lang
  FROM SIEBEL.S_CS_PATH sss
  INNER      JOIN siebel.s_cs_path_lang sssl ON sssl.path_id = sss.row_id
  LEFT OUTER JOIN SIEBEL.S_CS_EDGE T5 ON T5.PATH_ID = sss.Row_Id
  INNER      JOIN SIEBEL.S_CS_PAGE page     ON (T5.NEXT_PAGE_ID = page.ROW_ID OR sss.start_page_id = page.ROW_ID)
  LEFT OUTER JOIN SIEBEL.S_CS_EDGE T40      ON T40.PAGE_ID = page.row_id
  INNER      JOIN SIEBEL.S_CS_QUEST q       ON (T40.FROM_QUEST_ID = q.ROW_ID OR page.start_quest_id = q.ROW_ID)
  LEFT OUTER JOIN siebel.s_cs_quest_lang ql ON ql.quest_id = q.row_id
  INNER      JOIN siebel.s_cs_page_lang pl  ON pl.page_id = page.row_id )
SELECT DISTINCT SmartScript,
       max(greatest(LUPD_PATH_SCPT, LUPD_QUEST_SCPT, LUPD_ANSWR,  LUPD_PATH,   LUPD_EDGE1,  LUPD_PAGE,   LUPD_EDGE2,  LUPD_QUEST,  LUPD_QUEST_LANG)) AS max_d
       --s1.*
  FROM sss_changed s1
GROUP BY SmartScript
ORDER BY 2 DESC;