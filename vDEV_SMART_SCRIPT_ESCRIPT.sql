CREATE OR REPLACE VIEW vDEV_SMART_SCRIPT_ESCRIPT AS
SELECT qs.ROW_ID,
       'Frage' as type,
       'S_CS_QUEST_SCPT' as tbl,
       q.name as Object,
       qs.NAME,
       qs.last_upd,
       qs.SCRIPT

  FROM siebel.s_cs_quest_scpt qs
 inner join SIEBEL.S_CS_QUEST q
    on q.row_id = qs.quest_id
union all
SELECT ss.ROW_ID,
       'Script' as type,
       'S_CS_PATH_SCPT' as tbl,
       p.name as Object,
       ss.NAME,
       ss.last_upd,
       ss.SCRIPT
  FROM siebel.s_cs_path_scpt ss
 inner join SIEBEL.S_CS_PATH p
    on p.row_id = ss.path_id;
