CREATE OR REPLACE VIEW COLSTAT AS
SELECT c.owner,
       c.table_name,
       t.NUM_ROWS as table_count,
       c.column_name,
       t.NUM_ROWS - c.num_nulls AS col_count,
       DECODE(t.NUM_ROWS,
              0,
              0,
              DECODE(t.NUM_ROWS - c.num_nulls,
                     0,
                     0,
                     ROUND((t.NUM_ROWS - c.num_nulls) / (t.NUM_ROWS / 100), 0))) AS percent_filled,
       c.last_analyzed,
			 st.repository_id,
			 r.name as Repository,
			 cc.row_id as col_id,
			 cc.default_val,
			 cc.desc_text
  FROM sys.all_tab_col_statistics c
 INNER JOIN sys.all_tab_statistics t
    ON t.TABLE_NAME = c.TABLE_NAME
   AND t.OWNER = c.owner
	 and t.PARTITION_NAME is null
 left outer join S_TABLE st on st.name = c.table_name 
 left outer join S_REPOSITORY R on R.ROW_ID = st.repository_id
 left outer join s_Column cc on cc.tbl_id = st.row_id and c.column_name = cc.name
 WHERE c.owner = 'SIEBEL'
;


