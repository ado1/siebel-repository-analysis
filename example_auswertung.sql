SELECT DISTINCT x.*, bf.*
--, cs.num_filled 
FROM (
select DISTINCT ac.buscomp_name, ac.BC_ID, ac.FIELD , a.applet_name ,'AP'
from view2resp v
INNER JOIN app2view a ON a.View_id = v.View_ID
INNER JOIN appletcon ac ON ac.Applet_id = a.applet_id
INNER JOIN SCREEN2APP_SINGLE sa ON sa.VIEW_NAME = v.VIEW_NAME AND sa.REPOSITORY_ID = v.Repository_id
WHERE v.Resp LIKE 'LP%' AND v.Resp <> 'LP Siebel-Adminstrator'
AND a.repository = 'Siebel Repository'
AND ac.buscomp_name LIKE 'Contact'
/*UNION
SELECT --intobj  
DISTINCT bc, bc_id, bcfield AS field, intobj, 'IO'
FROM iofields
WHERE intobj LIKE 'LM%'
 AND bc = 'Contact' 
 AND repository = 'Siebel Repository'
 AND intobj NOT LIKE 'LMS DOC%'*/) x
 INNER join bcfields bf ON x.BC_ID = bf.BC_ID AND bf.field = x.field
INNER JOIN COLSTAT cs ON cs.table_name = bf.TAB AND cs.column_name = bf.col_name 
 
