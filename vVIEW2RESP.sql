DROP VIEW vDEV_VIEW2RESP;
CREATE VIEW vDEV_VIEW2RESP AS
	SELECT
		V.Name   AS VIEW_NAME,
		V.row_id AS View_ID,
		V.Repository_id,
		R.Name   AS Resp,
		R.Row_id AS Resp_id
	  FROM siebel.S_view V
INNER JOIN siebel.s_app_view av      ON av.name    = V.Name
INNER JOIN siebel.S_APP_VIEW_RESP RV ON RV.VIEW_ID = aV.row_id
INNER JOIN siebel.S_RESP R           ON R.ROW_ID   = RV.RESP_ID;